#####################################################
################ Clock Signals  (LVDS) ##############
#####################################################
set_property PACKAGE_PIN Y18 [get_ports clk_25m_vcxo_i]
set_property IOSTANDARD LVCMOS25 [get_ports clk_25m_vcxo_i]

#AUX CLK (OUT6 PLL). Without termination
#Line removed by Vivado.
set_property PACKAGE_PIN G3 [get_ports clk_aux_n_i]
set_property IOSTANDARD LVDS_25 [get_ports clk_aux_n_i]

#Dedicated GTP CLK. (OUT7 PLL)
#Line removed by Vivado
set_property PACKAGE_PIN E6 [get_ports gtp_dedicated_clk_n_i]

#Dedicated CLK to configure the AD9516 PLL (100 MHz by default). Without termination
#Line removed by Vivado
set_property PACKAGE_PIN W20 [get_ports clk_100mhz_n_i]
set_property IOSTANDARD LVDS_25 [get_ports clk_100mhz_n_i]

# The SERDES clock. AD9516 OUT 5.
#set_property PACKAGE_PIN C19 [get_ports clk_serdes_n_i]
#set_property IOSTANDARD LVDS_25 [get_ports clk_serdes_n_i]
#set_property PACKAGE_PIN C18 [get_ports clk_serdes_p_i]
#set_property IOSTANDARD LVDS_25 [get_ports clk_serdes_p_i]


#####################################################
#################### DAC Signals ####################
#####################################################
#DAC clk ref (PLL25DAC2_SYNC_N)
set_property PACKAGE_PIN L1 [get_ports dac_cs1_n_o]
set_property IOSTANDARD LVCMOS25 [get_ports dac_cs1_n_o]

#DAC clk dmtd (PLL25DAC1_SYNC_N)
set_property PACKAGE_PIN K6 [get_ports dac_cs2_n_o]
set_property IOSTANDARD LVCMOS25 [get_ports dac_cs2_n_o]

set_property PACKAGE_PIN M2 [get_ports dac_din_o]
set_property IOSTANDARD LVCMOS25 [get_ports dac_din_o]

set_property PACKAGE_PIN M3 [get_ports dac_sclk_o]
set_property IOSTANDARD LVCMOS25 [get_ports dac_sclk_o]

#####################################################
#################### 1-Wire Signal ##################
#####################################################
set_property PACKAGE_PIN P4 [get_ports thermo_id]
set_property IOSTANDARD LVCMOS25 [get_ports thermo_id]

#####################################################
#################### GTP Signals ####################
#####################################################
#MOD_DEF0 = SFPX_DETECT.
set_property PACKAGE_PIN F19 [get_ports gtp0_mod_def0_b]
set_property IOSTANDARD LVCMOS25 [get_ports gtp0_mod_def0_b]

set_property PACKAGE_PIN A20 [get_ports gtp0_mod_def1_b]
set_property IOSTANDARD LVCMOS25 [get_ports gtp0_mod_def1_b]

set_property PACKAGE_PIN A19 [get_ports gtp0_mod_def2_b]
set_property IOSTANDARD LVCMOS25 [get_ports gtp0_mod_def2_b]

#There is no rate_select in the WR-LEN SFPs
#set_property LOC  [get_ports gtp0_rate_select_b]

#LOS and tx_fault are shorted.
set_property PACKAGE_PIN B20 [get_ports gtp0_tx_fault_i]
set_property IOSTANDARD LVCMOS25 [get_ports gtp0_tx_fault_i]

set_property PACKAGE_PIN A18 [get_ports gtp0_tx_disable_o]
set_property IOSTANDARD LVCMOS25 [get_ports gtp0_tx_disable_o]

#LOS and tx_fault are shorted.
#set_property LOC K1 [get_ports gtp0_los_i]

#Lines removed by Vivado.
set_property PACKAGE_PIN C11 [get_ports gtp0_rxn_i]

#GTP1-
#MOD_DEF0 = SFPX_DETECT.
set_property PACKAGE_PIN C2 [get_ports gtp1_mod_def0_b]
set_property IOSTANDARD LVCMOS25 [get_ports gtp1_mod_def0_b]

set_property PACKAGE_PIN A1 [get_ports gtp1_mod_def1_b]
set_property IOSTANDARD LVCMOS25 [get_ports gtp1_mod_def1_b]

set_property PACKAGE_PIN B1 [get_ports gtp1_mod_def2_b]
set_property IOSTANDARD LVCMOS25 [get_ports gtp1_mod_def2_b]


#There is no rate_select in the WR-LEN SFPs
#set_property LOC  [get_ports gtp1_rate_select_b]

#LOS and tx_fault are shorted.
set_property PACKAGE_PIN K1 [get_ports gtp1_tx_fault_i]
set_property IOSTANDARD LVCMOS25 [get_ports gtp1_tx_fault_i]

set_property PACKAGE_PIN F4 [get_ports gtp1_tx_disable_o]
set_property IOSTANDARD LVCMOS25 [get_ports gtp1_tx_disable_o]

#LOS and tx_fault are shorted.
#set_property LOC  [get_ports gtp1_los_i]

#Lines removed by Vivado.
set_property PACKAGE_PIN A8 [get_ports gtp1_rxn_i]

# SFP LEDS
set_property PACKAGE_PIN B21 [get_ports gtp0_activity_led_o]
set_property IOSTANDARD LVCMOS25 [get_ports gtp0_activity_led_o]

#set_property PACKAGE_PIN  [get_ports gtp0_synced_led_o]

set_property PACKAGE_PIN B22 [get_ports gtp0_link_led_o]
set_property IOSTANDARD LVCMOS25 [get_ports gtp0_link_led_o]

#set_property PACKAGE_PIN  [get_ports gtp0_wrmode_led_o]

#GTP1
set_property PACKAGE_PIN A15 [get_ports gtp1_activity_led_o]
set_property IOSTANDARD LVCMOS25 [get_ports gtp1_activity_led_o]

#set_property PACKAGE_PIN  [get_ports gtp1_synced_led_o]

set_property PACKAGE_PIN A14 [get_ports gtp1_link_led_o]
set_property IOSTANDARD LVCMOS25 [get_ports gtp1_link_led_o]

#set_property PACKAGE_PIN  [get_ports gtp1_wrmode_led_o]


#####################################################
#################### I2C EEPROM #####################
#####################################################
set_property PACKAGE_PIN P1 [get_ports fpga_scl_b]
set_property IOSTANDARD LVCMOS25 [get_ports fpga_sda_b]

set_property PACKAGE_PIN R1 [get_ports fpga_sda_b]
set_property IOSTANDARD LVCMOS25 [get_ports fpga_scl_b]

#####################################################
#################### Reset Button ###################
#####################################################
set_property PACKAGE_PIN P2 [get_ports button_rst_i]
set_property PULLUP true [get_ports button_rst_i]
set_property IOSTANDARD LVCMOS25 [get_ports button_rst_i]

#####################################################
#################### LM32 UART ######################
#####################################################
#Lines inverted for the new LEN carrier board. (H2 J1 before)
set_property PACKAGE_PIN J1 [get_ports uart_rxd_i]
set_property IOSTANDARD LVCMOS25 [get_ports uart_rxd_i]
set_property PACKAGE_PIN H2 [get_ports uart_txd_o]
set_property IOSTANDARD LVCMOS25 [get_ports uart_txd_o]

#####################################################
################## EXT REF (GPS) ####################
#####################################################
set_property PACKAGE_PIN H4 [get_ports ext_clk_i]
set_property IOSTANDARD LVCMOS25 [get_ports ext_clk_i]
set_property PACKAGE_PIN M5 [get_ports pps_i]
set_property IOSTANDARD LVCMOS25 [get_ports pps_i]
set_property PACKAGE_PIN N5 [get_ports term_en_o]
set_property IOSTANDARD LVCMOS25 [get_ports term_en_o]

set_property PACKAGE_PIN P6 [get_ports pps_o]
set_property IOB TRUE [get_ports pps_o]
set_property IOSTANDARD LVCMOS25 [get_ports pps_o]

set_property PACKAGE_PIN P5 [get_ports pps_ctrl_o]
set_property IOSTANDARD LVCMOS25 [get_ports pps_ctrl_o]

#####################################################
#################### AD9516 SPI #####################
#####################################################
set_property PACKAGE_PIN F1 [get_ports pll_cs_n_o]
set_property IOSTANDARD LVCMOS25 [get_ports pll_cs_n_o]
set_property PACKAGE_PIN E1 [get_ports pll_sck_o]
set_property IOSTANDARD LVCMOS25 [get_ports pll_sck_o]
set_property PACKAGE_PIN E2 [get_ports pll_sdi_o]
set_property IOSTANDARD LVCMOS25 [get_ports pll_sdi_o]
set_property PACKAGE_PIN D1 [get_ports pll_sdo_i]
set_property IOSTANDARD LVCMOS25 [get_ports pll_sdo_i]
set_property PACKAGE_PIN E3 [get_ports pll_reset_n_o]
set_property IOSTANDARD LVCMOS25 [get_ports pll_reset_n_o]
set_property PACKAGE_PIN F3 [get_ports pll_status_i]
set_property IOSTANDARD LVCMOS25 [get_ports pll_status_i]
set_property PACKAGE_PIN B2 [get_ports pll_sync_n_o]
set_property IOSTANDARD LVCMOS25 [get_ports pll_sync_n_o]
set_property PACKAGE_PIN D2 [get_ports pll_refsel_o]
set_property IOSTANDARD LVCMOS25 [get_ports pll_refsel_o]
set_property PACKAGE_PIN G1 [get_ports pll_ld_i]
set_property IOSTANDARD LVCMOS25 [get_ports pll_ld_i]

####################################################
## Timing constraints
####################################################
create_clock -period 10.000 -name clk_100mhz_p_i -waveform {0.000 5.000} [get_ports clk_100mhz_p_i]
# create_clock -period 16.000 -name clk_serdes_p_i -waveform {0.000 8.000} [get_ports clk_serdes_p_i]
create_clock -period 8.000 -name clk_aux_p_i -waveform {0.000 4.000} [get_ports clk_aux_p_i]
create_clock -period 40.000 -name clk_25m_vcxo_i -waveform {0.000 20.000} [get_ports clk_25m_vcxo_i]
create_clock -period 100.000 -name ext_clk_i -waveform {0.000 50.000} [get_ports ext_clk_i]
create_clock -period 8.000 -name gtp_dedicated_clk_p_i -waveform {0.000 4.000} [get_ports gtp_dedicated_clk_p_i]

## Clocking groups.
set_clock_groups -asynchronous -group {clk_100mhz_p_i s_ext_mul_gated_clock}

set_clock_groups -asynchronous -group {clk_aux_p_i clk_aux pllout_clk_fb_aux pllout_clk_sys}

set_clock_groups -asynchronous -group {clk_25m_vcxo_i pllout_clk_dmtd}

set_clock_groups -asynchronous -group {ext_clk_i s_pllout_clk_fb_ext}

set_clock_groups -asynchronous -group gtp_dedicated_clk_p_i
